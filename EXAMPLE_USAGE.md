## Example usage

```text
$ echo 't&((t>>7)-t)&t>>8' | python ./bytebeat_compiler.py - -p 44100 --verbose
:: C bytebeat generator: compiler unit
Reading from STDIN...
Compiling
[>] cc -Ofast -march=native -mtune=native -Wall -Wextra -Wpedantic -pedantic -Wno-unused-variable -Wno-unused-but-set-variable -Wno-dangling-else -Wno-parentheses -std=c99 ./bin/substituted.c ./src/fwrite_le.c -o ./bin/render_bytebeat -I./include
[>] ./bin/render_bytebeat
:: C bytebeat generator runtime unit

Sample rate: 44100 Hz
Channels: 1 (mono)
Bit depth: unsigned 8-bit
Duration: 30 seconds

Writing WAVE headers...

Done!
```
